import socket
import ssl
import constants


# TODO :
#         Automatic detection of name for IMAP / SMTP server by shortcut string (
#       like gmail -> getter method GET_SMTP_NAME returns gmail.imap.com ...)


class Client:
    """
    Class description

    base on IMAP RFC 3501 (https://tools.ietf.org/html/rfc3501)
    """

    def __init__(self):

        self.__client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # AF_UNSPEC doesn't supported by SSL:(
        self.__client_socket.settimeout(0.5)

    def __change_to_SSL(self):
        self.__client_socket = ssl.wrap_socket(self.__client_socket, ssl_version=ssl.PROTOCOL_SSLv23)

    def initialize_connection_params(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        self.__change_to_SSL()

        try:
            self.__client_socket.connect((kwargs['server'], kwargs['port']))
            self.__start_connection(kwargs['name'], kwargs['password'])
        except Exception as e:
            print(e)
        finally:
            # print('No problems, I\'ll close this')
            self.__client_socket.close()

    def __start_connection(self, login, password):

        server_msg = self.__socket_read()

        if not server_msg.startswith('* OK'):
            raise ConnectionAbortedError(constants.WRONG_CONNECTION)

        auth_data = "? LOGIN {0} {1}\r\n".format(login, password)
        print(auth_data)

        self.__client_socket.send(auth_data.encode())
        server_answer = self.__socket_read()
        print(server_answer)

        if server_answer.startswith('? NO'):
            raise ValueError(constants.WRONG_AUTHORIZATION_DATA)

        # print(server_answer)

    def __socket_read(self):

        buff = []

        while True:
            try:
                data = self.__client_socket.recv(1)
                buff.append(data)
            except socket.error:
                break

        # while True:
        #
        #     data = self.__client_socket.recv(1)
        #
        #     buff.append(data)
        #     print(data)
        #      # if (len(buff) > 2) and (buff[len(buff)-1] == b'\n') and (buff[len(buff)-2] == b'\r'):
        #      #     break
        #
        # print(buff)
        return self.__decoded_answer(b''.join(buff))


    def __decoded_answer(self, byte_string):
        return "".join(map(chr, byte_string))