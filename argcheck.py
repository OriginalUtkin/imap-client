import constants
import re
import os


class ArgumentChecker:

    def __init__(self, **kwargs):

        self.auth_string = kwargs['ad']
        self.auth_file = kwargs['af']
        self.output_dir = kwargs['output']
        self.port = kwargs['port']

    def check_arguments(self):

        # Check authorization file or authorization string (is readable; exist; format)
        if self.auth_file is not None:
            self.__check_auth_file(self.auth_file)
        else:
            self.__check_auth_string(self.auth_string)

        # Check output dir (is readable / writeable ; directory exists)
        if self.output_dir is not None:
            self.__check_output_dir(self.output_dir)

        self.__checkPort(self.port)

    def __checkPort(self, port_value):
        if (constants.MIN_PORT_VALUE > port_value) or (port_value > constants.MAX_PORT_VALUE):
            raise Exception(constants.WRONG_PORT_VALUE)

    def __check_auth_file(self, file_path):
        with open(file_path,"r") as file_stream:
                data = [line for line in file_stream]
                if (len(data) != 2) or (not self.__check_string_pattern(constants.EMAIL_PATTERN, data[0].rstrip())):
                    raise Exception(constants.WRONG_FILE_FORMAT)

    def __check_output_dir(self, path):
        if not os.access(path, os.R_OK):
            raise Exception(constants.WRONG_OUTPUT_PATH)

    def __check_auth_string(self, auth_string):
        if not(self.__check_string_pattern(constants.AUTH_STRING_PATTERN, auth_string)):
            raise Exception(constants.WRONG_AUTH_STRING_FORMAT)

    def __check_string_pattern(self, string_pattern, email_string):
        if re.search(string_pattern, email_string) is not None:
            return True