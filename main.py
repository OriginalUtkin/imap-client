import argparse as ap
import argcheck as ac
import constants


# TODO:
#       Message for send if operation is SEND

def argument_parse():
    """

    :return:
    """
    parser = ap.ArgumentParser()

    parser.add_argument('server', help='Name of e-mail server')
    parser.add_argument('command', choices=['send', 'read', 'check'], help='Name of action (send, read or check)')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-af', help='Authorization file with login and password')
    group.add_argument('-ad',
                       help='Authorization data for connection to e-mail server, as with a format like Name##Pass')

    parser.add_argument('-p', '--port', help='Port number for communication with e-mail server', type=int)
    parser.add_argument('-o', '--output', help='Output directory path (folder with script on default)')

    return vars(parser.parse_args())


def prepare_arguments(**kwargs):
    """

    :param kwargs:
    :return:
    """
    # Create new dictionary with CONNECTION_ARGUMENTS keys
    prepared_arg = dict.fromkeys(constants.CONNECTION_ARGUMENTS)

    # Find out name and password for authorization from command-line arguments
    if kwargs['ad'] is not None:
        name, password = get_name_from_string(kwargs['ad'])
    else:
        name, password = get_name_from_file(kwargs['af'])

    # If port doesn't defined, choose correct port value using operation name
    if kwargs['port'] is None:
        kwargs['port'] = set_port_value(kwargs['command'])

    # Copy key and value from kwargs to prepared_arg if key exists in prepared_arg dict
    prepared_arg = {key: kwargs.get(key) if key in kwargs else None for key, value in prepared_arg.items()}
    prepared_arg['name'] = name
    prepared_arg['password'] = password

    return prepared_arg


def set_port_value(operation_name):
    """

    :param operation_name:
    :return:
    """
    if operation_name == 'send':
        return constants.DEFAULT_SMTP_PORT
    else:
        return constants.DEFAULT_IMAP_PORT


def get_name_from_file(auth_file):
    """

    :param auth_file:
    :return:
    """
    with open(auth_file) as file_stream:
        return [file_line.strip() for file_line in file_stream]


def get_name_from_string(auth_string):
    """

    :param auth_string:
    :return:
    """
    return auth_string[:auth_string.find('##')].rstrip('##'), \
           auth_string[auth_string.rfind('##'):].lstrip('##')


if __name__ == '__main__':

    arguments = argument_parse()
    checker = ac.ArgumentChecker(**arguments)

    try:
        checker.check_arguments()
    except Exception as exc:
        print(exc)
        exit(1)
    else:  # Communicate with server
        prepared_arguments = prepare_arguments(**arguments)
        print(prepare_arguments(**arguments))
