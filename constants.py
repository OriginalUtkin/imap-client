# Constants for check with
MIN_PORT_VALUE = 0
MAX_PORT_VALUE = 65535


# Default values using in program
DEFAULT_IMAP_PORT = 993
DEFAULT_SMTP_PORT = 465

# Keys for connection arguments dictionary in Main module
CONNECTION_ARGUMENTS = ['server', 'command', 'name', 'password', 'port', 'output']


# Regex patterns
AUTH_STRING_PATTERN = r"[^@]+##[^@]+"
EMAIL_PATTERN = r"[^@]+@[^@]+\.[^@]+"


# Exception messages
WRONG_PORT_VALUE = '[Error]: Wrong value of port. Choose value from 0 to 65535'
WRONG_FILE_FORMAT = '[Error]: Wrong authorization file format'
WRONG_AUTH_STRING_FORMAT = '[Error]: Wrong authorization string format (af parametr)'
WRONG_OUTPUT_PATH = '[Error]: Something wrong with output path'
WRONG_AUTHORIZATION_DATA = '[Error]: Wrong authorization data on IMAP server'
WRONG_CONNECTION = '[Error]: IMAP server is unreachable'
